@extends('layouts.main')

@section('title', 'Viaggi in Italia e nel mondo')

@section('body')

@isset($title)
<header>
	<div class="title">
		{!! $title !!}
		@isset($subtitle)
		<div class="subtitle">{!! $subtitle !!}</div>
		@endisset
	</div>
</header>
@endisset

<section class="content" id="our-offers">
	<div class="row">
@for ($i = 0; $i < count($data); $i++)
	@if ($i % 4 == 0)
	</div>
	<div class="row">
	@endif
		<a class="package-preview" href="/package/{{ $data[$i]->id }}">
			<img src="{{ url($data[$i]->photo) }}">
			<div class="description">
				<div class="title">{{ $data[$i]->name }}</div>
				<div class="subtitle">
					<div class="price">€{{ $data[$i]->price }}</div>
					<!--<div class="reviews">4.2 <span class="stars">★★★★✩</span></div>-->
				</div>
				<div class="button">H<span class="letter-a">a</span>UNT!</div>
			</div>
		</a>
@endfor
	</div>
</section>

@endsection
