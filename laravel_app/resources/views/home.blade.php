@extends('layouts.main')

@section('title', 'Viaggi in Italia e nel mondo')

@section('body')
<!--
<script>
	newsletter.show();
</script>
-->

<header>
	<img src="images/logo-big.png"/>
	<div class="title">
		Rental Bus Italy
		<div class="subtitle">We offer high quality transfers and rental services of GT coaches with drivers throughout Italy.</div>
	</div>
</header>

<section class="content" id="our-offers">
	<!-- <h1>Le nostre offerte</h1> -->

	<div class="row">
		<a class="category" href="{{ url("cheap-n-fun") }}">
			<img src="images/cheap-n-fun.jpeg" />
			<div class="curtain">Services</div>
		</a>

		<a class="category" href="{{ url("go-wild") }}">
			<img src="images/go-wild2.jpg" />
			<div class="curtain">Contact us</div>
		</a>
	</div>

	<div class="row">
		<a class="category" href="{{ url("around-the-world") }}">
			<img src="images/around-the-world.jpg" />
			<div class="curtain">Our fleet</div>
		</a>
	</div>
</section>
@endsection