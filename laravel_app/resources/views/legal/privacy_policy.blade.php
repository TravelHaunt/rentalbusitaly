@extends('layouts.main')

@section('title', 'Viaggi in Italia e nel mondo')

@section('body')
<header>
	<div class="title">
		<span style="color: #7d0bed;">P</span>rivacy Policy
		<div class="subtitle"></div>
	</div>
</header>
<section>
		<h2>Disclaimer</h2>

		<p>Ultimo aggiornamento: 01 luglio 2017</p>

		<p>Le informazioni contenute sul sito web www.travelhaunt.it (il "Servizio") sono
		Solo per scopi informativi generali.</p>

		<p>Travel Haunt non si assume alcuna responsabilità per errori o omissioni nei contenuti
		Sul Servizio.</p>

		<p>In nessun caso Travel Haunt sarà responsabile per Danni di qualsiasi tipo speciale, diretto, indiretto,
		consequenziali o incidentali o eventuali, sia per Danni in un
		Azione di contratto, negligenza o altri comportamenti derivanti da o in connessione
		Con l'utilizzo del Servizio o del contenuto del Servizio. Travel Haunt
		Si riserva il diritto di apportare aggiunte, cancellazioni o modifiche al
		I contenuti del Servizio in qualsiasi momento senza preavviso.</p>

		<p>Travel Haunt garantisce che il sito web sia privo di virus o altri
		Componenti nocivi.</p>

		<p>Questa Disclaimer è concessa in licenza da [TermsFeed Generator] (https://termsfeed.com) a
		Travel Haunt.</p>

		<h2> Politica sulla riservatezza </h2>

		<p>Travel Haunt gestisce il sito www.travelhaunt.it (il "Servizio").</p>

		<p>Questa pagina ti informa delle nostre norme in materia di raccolta, utilizzo e
		La divulgazione delle informazioni personali quando si utilizza il nostro Servizio.</p>

		<p>Non utilizzeremo o condividiamo le tue informazioni con nessuno tranne per ciò che descritto in
		Questa politica sulla privacy. Questa Politica sulla Privacy è concessa in licenza da [TermsFeed
		Generator] (https://termsfeed.com) a Travel Haunt.</p>

		<p>Usiamo le tue informazioni personali per fornire e migliorare il Servizio.
		Utilizzando il Servizio, accetti la raccolta e l'utilizzo delle informazioni in
		Conformità a questa politica. Salvo diversa definizione di questa politica sulla privacy,
		I termini utilizzati in questa Politica sulla Privacy hanno gli stessi significati dei nostri Termini e
		Condizioni accessibili all'indirizzo www.travelhaunt.it</p>
		

		<h2>Raccolta e utilizzo di informazioni</h2>

		<p>Durante l'utilizzo del nostro servizio, ti chiediamo di fornirci alcune informazioni personali 
		che possono essere utilizzate per contattarti o identificarti.</p>
		<p>Le informazioni personali possono includere, ma non sono limitate a, il tuo
		Nome, numero di telefono, indirizzo postale ("Informazioni personali").</p>


		<h2>Dati del registro</h2>

		<p>Riceviamo informazioni che il tuo browser invia ogni volta che visiti il nostro Servizio
		("Dati del registro"). Questi dati di registro possono includere informazioni come il
		Protocollo Internet del tuo Computer (IP), il tipo di browser, versione browser, le pagine del
		nostro servizio che visiti, l'ora e la data della tua visita, il tempo trascorso in
		quelle pagine e altre statistiche.</p>


		<h2>Cookies</h2>

		<p>I cookie sono file con una minima quantità di dati, che possono includere un identificativo anonimo
		unico. I cookies vengono inviati al tuo browser da un sito web e memorizzati
		Sul disco rigido del computer.</p>

		<p>Usiamo i "cookie" per raccogliere informazioni. Puoi impostare il tuo browser a
		rifiutare tutti i cookie o indicare quando viene inviato un cookie. Tuttavia, se
		non accetterai i cookies, potresti non essere in grado di utilizzare alcune parti del nostro
		Servizio.</p>


		<h2>Fornitori di servizi</h2>

		<p>Possiamo impiegare aziende e privati ??di terze parti per facilitare il nostro servizio,
		o per fornire il Servizio per conto nostro, per eseguire servizi connessi al Servizio o
		Per aiutarci nell'analizzare come viene utilizzato il nostro servizio.</p>

		<p>Queste terze parti hanno accesso ai tuoi dati personali solo per eseguire
		questi compiti per nostro conto e sono obbligati a non divulgarli o utilizzarli per nessuno
		Altro scopo.</p>


		<h2>Sicurezza</h2>

		<p>La sicurezza delle tue informazioni personali è importante per noi, ma ricorda
		che nessun metodo di trasmissione su Internet, o metodo di archiviazione elettronica
		è al 100% sicuro. Mentre ci sforziamo di usare mezzi commercialmente accettabili
		per proteggere le tue informazioni personali, non possiamo garantire l'assoluta
		sicurezza.</p>


		<h2>Collegamenti ad altri siti</h2>

		<p>Il nostro Servizio può contenere collegamenti ad altri siti non gestiti da noi. Se
		Si fa clic su un collegamento di terze parti, sarai indirizzato al sito di quella terza parte.
		Ti consigliamo vivamente di esaminare la Privacy Policy di ogni sito
		che visiti.</p>

		<p>Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per il contenuto, le Politiche di riservatezza
		o pratiche di siti o servizi di terze parti.</p>


		<h2>Privacy dei bambini</h2>

		<p>Il nostro Servizio non è rivolto a nessuno sotto i 13 anni ("Bambini").</p>

		<p>Noi non possiamo sapere se stiamo raccogliedo informazioni personali da parte dei bambini
		Sotto 13. Se sei un genitore o un tutore e sai che i tuoi figli
		ci ha fornito informazioni personali, contattaci. Se scopriremo
		Che bambini sotto i 13 anni ci hanno fornito informazioni personali,
		elimineremo immediatamente tali informazioni dai nostri server.</p>


		<h2>Modifiche a questa politica sulla privacy</h2>

		<p>Possiamo aggiornare di volta in volta la nostra Politica sulla Privacy. Vi informeremo di qualsiasi altra
		modifica pubblicando la nuova politica sulla privacy su questa pagina.</p>

		<p>Si consiglia di esaminare periodicamente la presente Informativa sulla privacy per eventuali modifiche.
		Le modifiche di questa politica sulla privacy sono efficaci quando vengono pubblicate su questo pagina.
		</p>


		<h2>Contattaci</h2>

		<p>Se hai domande su questa politica sulla privacy, contattaci.</p>
	</section>
@endsection