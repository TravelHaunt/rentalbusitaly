@extends('layouts.main')

@section('title', 'Viaggi in Italia e nel mondo')

@section('body')
<header>
	<div class="title">
		Contact Us
	</div>
</header>

<section class="contact-us">
	<h2>Call or contact via WhatsApp our operators:</h2>

	<div class="phone-list">
		<div class="phone">Andrea +39 327 321 8017</div>
		<div class="phone">Gabriele +39 331 901 3911</div>
		<div class="phone">Olga +39 345 695 1684</div>
	</div>

	<h2>Send us a mail:</h2>
	<span class="email">info@rentalbusitaly.com</span>

	<h2>Or write direttamente:</h2>

	@if ($errors->any())
	@foreach ($errors->all() as $error)
	<div class="error">{{ $error }}</div> 
	@endforeach
	@endif
	
	<form method="post" action="/contact-us">
		{!! csrf_field() !!}
		<input type="text" placeholder="Name" name="name" value="{{ old('name') }}" />
		<input type="text" placeholder="E-mail" name="email" value="{{ old('email') }}" />
		<textarea rows="4" name="notes">{{ old('notes') }}</textarea>
		<button class="button" type="submit">Send</button>
	</form>
</section>
@endsection