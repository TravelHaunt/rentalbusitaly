@extends('layouts.main')

@section('title', 'Viaggi in Italia e nel mondo')

@section('body')
<div class="popup-wrapper" id="popup-guide">
  <form class="popup" method="post" action="{{ url("newsletter") }}" onsubmit="NewsletterAJAXSubmit(this); return false;">
    {!! csrf_field() !!}
    <div class="close" id="popup-guide-close">X</div>
    <div id="popup-guide-content">
      <div class="title">Ciao!</div>
      <div class="content">
        Iscriviti e scarica gratis la guida alle <br />"10 città imperdibili per il 2017"
      </div>

      <input type="text" placeholder="Nome" name="name" />
      <input type="text" placeholder="peter.parker@mail.com" name="email" />
      <button class="button">ISCRIVITI!</button>
    </div>
  </form>
</div>

<header>
  <img src="images/logo-big.png"/>
  <div class="title">
    TRAVEL H<span style="color: #7d0bed;">a</span>UNT
    <div class="subtitle">Qualsiasi tipo di esperienza tu stia cercando, noi te la offriamo.</div>
  </div>
</header>

<section class="search">
  <form>
    <div class="block">
      <div class="title">La tua destinazione</div>
      <input type="text" placeholder="Il mondo" />
    </div>

    <div class="block">
      <div class="title">Quando</div>
      <input type="date" placeholder="dd/mm/yyyy" />
    </div>

    <div class="block">
      <div class="title">Nr. persone</div>
      <input type="text" placeholder="Più siete, meglio è" />
    </div>

    <button>HaUNT!</button>
  </form>

  <div class="arrow">
    <a href="#our-offers"><img id="go-down" src="images/arrow.png"></a>
  </div>
</section>

<section class="content" id="our-offers">
  <h1>Le nostre offerte</h1>

  <div class="row">
    <a class="category" href="{{ url("cheap-n-fun") }}">
      <img src="images/cheap-n-fun.jpeg" />
      <div class="curtain">Cheap'n'Fun</div>
    </a>

    <a class="category" href="{{ url("go-wild") }}">
      <img src="images/go-wild2.jpg" />
      <div class="curtain">Go Wild</div>
    </a>
  </div>

  <div class="row">
    <a class="category" href="{{ url("around-the-world") }}">
      <img src="images/around-the-world.jpg" />
      <div class="curtain">Around the world</div>
    </a>

    <a class="category" href="{{ url("eventi") }}">
      <img src="images/eventi2.jpg" />
      <div class="curtain">Eventi</div>
    </a>
  </div>
</section>
@endsection
