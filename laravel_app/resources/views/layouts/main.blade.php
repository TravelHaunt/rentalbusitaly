<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Rental Bus Italy</title>

		<link rel="stylesheet" type="text/css" href="{{ url("/css/fonts.css") }}" />
		<link rel="stylesheet" type="text/css" href="{{ url("/css/main.css") }}" />

		<script src="{{ url("/js/app.js") }}"></script>
	</head>

	<body>
		<nav>
			@include('components.navigation')
		</nav>

		<div class="popup-wrapper" id="popup-guide"></div>

		<div class="cookie-popup" id="cookie-popup">
			<div class="close" id="cookie-popup-cross">X</div>
			<div class="content">
				Questo sito o gli strumenti terzi da questo utilizzati si avvalgono di cookie necessari al funzionamento ed utili alle finalità illustrate nella cookie policy. Se vuoi saperne di più o negare il consenso a tutti o ad alcuni cookie, consulta la <a href="#">cookie policy</a>.<br/>
				<br />
				Chiudendo questo banner, scorrendo questa pagina, cliccando su un link o proseguendo la navigazione in altra maniera, acconsenti all'uso dei cookie.
			</div>
			<div class="button" id="cookie-popup-button">OK!</div>
		</div>

		<div class="wrapper">
			@section('body')
				Body
			@show
		</div>

		<footer class="main">
			@include('components.footer')
		</footer>
	</body>
</html>