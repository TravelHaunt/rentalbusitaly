@isset($nav_logo)
<div class="logo">
	<img src="{{ url("images/logo-50x50.png") }}" />
</div>
@endisset

@empty($nav_logo)
<div class="phone">
	+39 331 901 3911
</div>
@endempty

<ul>
	<a href="{{ url("/") }}"><li>Home</li></a>
	<a href="{{ url("/about") }}"><li>Services</li></a>
	<a href="{{ url("/about") }}"><li>Request a quote</li></a>
	<a href="{{ url("/contact-us") }}"><li>Contact us</li></a>
</ul>

<!--
<ul class="social">
	<li><a href="https://www.facebook.com/travelhauntviaggi"><img src="{{ url("/images/social/001-social-media.svg") }}"/></a></li>
	<li><img src="{{ url("/images/social/469304.svg") }}" onclick="newsletter.show()"></li>
	<li><img src="{{ url("/images/social/124032.svg") }}"/></li>
</ul>
 -->