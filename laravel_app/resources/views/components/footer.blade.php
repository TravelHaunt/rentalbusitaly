<div class="flex-wrapper wrapper">
	<img src="{{ url("images/logo-big.png") }}" />
	<div class="contacts">
		<p>
			<b>For info or quotations:</b>
			<div class="phone">Gabriele: +39 331 901 3911</div>
			<div class="phone">Olga: +39 345 695 1684</div>
		</p>

		<p>
			<b>Or send us a mail:</b>
			<div class="mail">
				info@rentalbusitaly.com
			</div>
		</p>
	</div>

	<div class="legal">
		<div class="iva">
			Counting house: Via della Repubblica 8, 56029 Santa Croce sull'Arno (PI)<br/>
			Tel. +39 348 2804367 mail: mariodisipio@yahoo.it<br/>
			Powered by Acacia Travel Società Cooperativa<br/>
			P. IVA 01173420868 - Italy Licence n. 229/S.9
		</div>
	
		<div class="copyright">
			<div class="element">© Icon made by Freepik from www.flaticon.com</div>
			<div class="element">© Photos by Fotolia.com</div>
			<div class="element">© Photos by Shutterstock.com</div>
		</div>
	</div>
</div>

<div class="others">
	<a href="{{ url("/legal/privacy") }}">Privacy</a>
	<a href="{{ url("/legal/terms-and-conditions") }}">Termini & Condizioni</a>
</div>