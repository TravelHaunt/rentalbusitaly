
<div class="popup">
	<div class="close" id="popup-guide-close">X</div>

	<div id="popup-guide-content">
		<div class="title">Grazie!</div>
		<div class="content">
			Entro 24 ore riceverai il tuo regalo.<br/>
			Continua la visita nel nostro sito e scopri le migliori offerte!
		</div>
	</div>
</div>