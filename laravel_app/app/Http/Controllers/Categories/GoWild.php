<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\CategoryController;

class GoWild extends CategoryController
{
	public function __construct()
	{
		$this->nav_logo = TRUE;
		$this->category_id = 2;
		$this->title = 'Go <span style="color: #7d0bed;">WILD</span>';
		$this->subtitle = NULL;
	}
}
