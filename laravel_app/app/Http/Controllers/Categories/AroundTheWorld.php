<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\CategoryController;

class AroundTheWorld extends CategoryController
{
	public function __construct()
	{
		$this->nav_logo = TRUE;
		$this->category_id = 3;
		$this->title = 'Around the <span style="color: #7d0bed;">WORLD</span>';
		$this->subtitle = NULL;
	}
}
