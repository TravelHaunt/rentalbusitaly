<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Http\Requests\ContactFormRequest;

class MainController extends Controller
{
	public function index()
	{
		return view('home');
	}

	public function contact()
	{
	    return view("contact-us", [
	    	"nav_logo" => TRUE
	    ]);
	}

	public function contactPost(ContactFormRequest $request)
	{
		DB::table('contacts')->insert([
			'name' => $request->input('name'),
			'email' => $request->input('email'),
			'notes' => $request->input('notes')
		]);

	    return view('success', [
	    	'nav_logo' => TRUE
	    ]);
	}

	public function about()
	{
		return view("about-us", [
			"nav_logo" => TRUE
		]);
	}
	
    public function privacyPolicy()
    {
        return view("legal.privacy_policy", [
			"nav_logo" => TRUE
		]);
    }

    public function termsAndConditions()
    {
        return view("legal.terms_and_conditions", [
			"nav_logo" => TRUE
		]);
    }
}
