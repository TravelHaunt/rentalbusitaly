<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
	public function showCategoryPackages()
	{
		return view("preview", [
			"nav_logo" => $this->nav_logo,
			"title" => $this->title,
			"subtitle" => $this->subtitle,
			"data" => $this->getCategoryPackages()
		]);
	}

	function getCategoryPackages()
	{
		$data = DB::table("packages")->select("id", "name", "price", "photo")->where("category_id", $this->category_id)->get();

		return $data;
	}
}
