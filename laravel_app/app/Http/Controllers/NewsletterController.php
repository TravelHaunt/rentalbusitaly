<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\NewsletterFormRequest;

class NewsletterController extends Controller
{
	public function index()
	{
		return view("components.newsletter");
	}

	public function submit(Request $request) 
	{
		$input = $request->all();

		$validator = Validator::make($input, [
			'name' => 'required',
			'email' => 'required|email'
		], [
			'name.required' => 'Il campo nome non può essere lasciato vuoto.',
			'email.required' => 'Il campo email non può essere lasciato vuoto.',
			'email.email' => 'Indirizzo email non valido.'
		]);

		if ($validator->fails()) {
			return redirect('newsletter')->withErrors($validator)
				->withInput();
		}

		DB::table('newsletter')->insert([
			'name' => $input['name'],
			'email' => $input['email']
		]);

		return view('components.newsletter-success');
	}
}
