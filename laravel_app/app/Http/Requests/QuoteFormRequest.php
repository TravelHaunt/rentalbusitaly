<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuoteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'n_people' => 'required|integer',
            'name' => 'required|max:100',
            'surname' => 'required|max:100',
            'email' => 'required|email|max:100',
            'notes' => 'required',

            'terms' => 'accepted',
            'personal' => 'accepted'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Il campo nome non può essere lasciato vuoto.',
            'surname.required' => 'Il campo cognome non può essere lasciato vuoto.',
            'email.required' => 'Il campo email non può essere lasciato vuoto.',
            'email.email' => 'Indirizzzo email non valido.',
            'n_people.required' => '"In quanti partite" è un campo obbligatorio!',
            'n_people.integer' => '"In quanti partite" deve essere un numero!',
            'notes.required' => 'La tua richiesta non può essere vuota!',
            
            'terms.accepted' => 'Devi accettare i termini e condizioni!',
            'personal.accepted' => 'Devi accettare il trattamento dei dati personali!'
        ];
    }
}
