<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'name' => 'required',
            'email' => 'required|email',
            'notes' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Il campo nome non può essere lasciato vuoto.',
            'email.required' => 'Il campo email non può essere lasciato vuoto.',
            'email.email' => 'Indirizzzo email non valido.',
            'notes.required' => 'La tua richiesta non può essere vuota!'
        ];
    }
}
