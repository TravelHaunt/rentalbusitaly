<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'n_people' => 'required|integer|min:1',

            'name' => 'required|max:100',
            'surname' => 'required|max:100',

            //'birthday' => 'required|max:30',
            //'fiscal_code' => 'required|max:11|min:11',

            'email' => 'required|email|max:100',

            'terms' => 'accepted',
            'personal' => 'accepted'
        ];
    }

    public function messages()
    {
    	return [
            'n_people.required' => '"In quanti partite" è un campo obbligatorio!',
            'n_people.integer' => '"In quanti partite" deve essere un numero!',
            'n_people.min' => 'Il numero di persone deve essere almeno uno.',

            'name.required' => 'Il campo nome non può essere lasciato vuoto.',
            'surname.required' => 'Il campo cognome non può essere lasciato vuoto.',

            // 'birthday.required' => 'Il campo data di nascita non può essere lasciato vuoto.',
            // 'fiscal_code.required' => 'Il campo codice fiscale non può essere lasciato vuoto.',
            // 'fiscal_code.max:11' => 'Il codice fiscale deve essere di 11 caratteri.',
            // 'fiscal_code.min:11' => 'Il codice fiscale deve essere di 11 caratteri.',

            'email.required' => 'Il campo email non può essere lasciato vuoto.',
            'email.email' => 'Indirizzzo email non valido.',
            
    		'terms.accepted' => 'Devi accettare i termini e condizioni!',
    		'personal.accepted' => 'Devi accettare il trattamento dei dati personali!'
    	];
    }
}
